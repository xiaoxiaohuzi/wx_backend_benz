package com.backend.constants;

/**
 * @author HUJIAMIN
 * @since 2020/11/6
 */
public class WxConstant {

  /**
   * 微信请求URL
   */
  public static final String WX_URL_FORMAT = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";


}
