package com.backend;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xiaohuzi
 */
@SpringBootApplication
@Slf4j
@MapperScan("com.backend.web.*.mapper")
public class DemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
    log.info(">>>>> wisdom-web 启动完成 <<<<<");
  }

}
