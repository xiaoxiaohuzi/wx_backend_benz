package com.backend.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author HUJIAMIN
 * @since 2020/11/6
 */

@Data
@Configuration
public class WxConfig {

  @Value("${weixin.appid}")
  private String appid;

  @Value("${weixin.secret}")
  private String secret;

}
