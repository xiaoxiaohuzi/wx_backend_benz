package com.backend.web.weixin.controller;

import com.alibaba.fastjson.JSONObject;
import com.backend.web.weixin.service.IWxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HUJIAMIN
 * @since 2020/11/6
 */
@RestController
@RequestMapping("/wx")
public class WxController {

  @Autowired
  private IWxService wxService;

  @GetMapping("/code2Session")
  public JSONObject code2Session(String code) {
    return wxService.code2Session(code);
  }

  @PostMapping("/getUserInfo")
  public JSONObject getUserInfo(@RequestBody JSONObject json) {
    return wxService.getUserInfo(json);
  }

  /**
   * 登录接口
   *
   * @param json
   * @return
   */
  @PostMapping("/login")
  public JSONObject login(@RequestBody JSONObject json) {
    return wxService.login(json);
  }

  /**
   * 登录接口
   *
   * @param json
   * @return
   */
  @PostMapping("/loginByCode")
  public JSONObject loginByCode(@RequestBody JSONObject json) {
    return wxService.loginByCode(json);
  }

}
