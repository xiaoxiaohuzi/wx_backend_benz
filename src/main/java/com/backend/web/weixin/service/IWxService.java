package com.backend.web.weixin.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author HUJIAMIN
 * @since 2020/11/7
 */
public interface IWxService {

  /**
   * 登录凭证校验。通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程
   *
   * @param code 登录时获取的 code
   * @return
   */
  JSONObject code2Session(String code);

  /**
   * 获取用户信息(解析微信的加密请求参数，进行解密)
   *
   * @param json
   * @return
   */
  JSONObject getUserInfo(JSONObject json);

  JSONObject loginByCode(JSONObject json);

  /**
   * 登录
   *
   * @param json
   * @return
   */
  JSONObject login(JSONObject json);

}
