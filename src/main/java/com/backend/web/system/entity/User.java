package com.backend.web.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author HUJIAMIN
 * @since 2020-11-07
 */
@Data
@EqualsAndHashCode
@Accessors(chain = true)
public class User {

  private static final long serialVersionUID = 1L;

  @TableId(value = "USER_ID", type = IdType.UUID)
  private String userId;

  @TableField("OPEN_ID")
  private String openId;

  @TableField("PHONE")
  private String phone;

  @TableField("PASSWORD")
  private String password;

  @TableField("URL")
  private String url;


}
