package com.backend.web.system.mapper;

import com.backend.web.system.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author HUJIAMIN
 * @since 2020-11-07
 */
public interface UserMapper extends BaseMapper<User> {

}
