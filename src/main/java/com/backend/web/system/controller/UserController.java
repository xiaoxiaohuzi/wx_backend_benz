package com.backend.web.system.controller;

import com.backend.web.system.entity.User;
import com.backend.web.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author HUJIAMIN
 * @since 2020-11-07
 */
@RestController
@RequestMapping("/system/user")
public class UserController {

  @Autowired
  private IUserService userService;

  @GetMapping("/page")
  public Page<User> page(Page<User> page) {
    QueryWrapper<User> queryWrapper = new QueryWrapper<>();
    userService.page(page);
    return page;
  }

  @GetMapping("/query")
  public User query(String openid) {
    return userService.getByOpenId(openid);
  }

}