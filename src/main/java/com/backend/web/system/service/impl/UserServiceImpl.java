package com.backend.web.system.service.impl;

import com.backend.web.system.entity.User;
import com.backend.web.system.mapper.UserMapper;
import com.backend.web.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author HUJIAMIN
 * @since 2020-11-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

  @Override
  public User getByOpenId(String openId) {
    return this.getOne(new LambdaQueryWrapper<User>().eq(User::getOpenId, openId));
  }

  @Override
  public boolean addUser(String openId, String phone) {
    User user = getByOpenId(openId);
    if (user != null) {
      return false;
    } else {
      user = new User();
      user.setOpenId(openId);
      user.setPhone(phone);
      save(user);
      return true;
    }
  }
}
