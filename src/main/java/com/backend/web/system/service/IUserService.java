package com.backend.web.system.service;

import com.backend.web.system.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author HUJIAMIN
 * @since 2020-11-07
 */
public interface IUserService extends IService<User> {

  User getByOpenId(String openId);

  boolean addUser(String openId, String phone);

}
