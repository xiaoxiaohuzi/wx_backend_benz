package com.backend.web;

import com.backend.utils.HttpUtil;

/**
 * @author HUJIAMIN
 * @since 2020/11/6
 */
public class Test {

  private static final String appid = "wx675725ab7515e354";
  private static final String secret = "09c55ce1204b554f1b38263bb45613ac";
  private static final String WX_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";


  public static void main(String[] args) throws Exception {
    String url = String.format(WX_URL, appid, secret, "0039iK0w366fgV23h64w3VCvE339iK0i");
    System.out.println(url);

    String res = HttpUtil.get(url);
    System.out.println(res);
  }
}
